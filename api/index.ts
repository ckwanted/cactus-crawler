// MARK: - Modules
import dotenv from "dotenv";
dotenv.config();

import express from "express";
import cron from "node-cron";
import path from 'path';
import bodyParser from "body-parser";
import compression from "compression";
import helmet from "helmet";
import morgan from "morgan";
import cors from "cors";
import log from "./libs/winston/logger";
import Idrometeo from "./libs/puppeteer/Idrometeo";

const app = express();
app.set('views', path.join(__dirname, '..', 'views'));
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw({ type: 'image/*', limit: '10mb' }));
app.use(
    morgan('short', {
        stream: {
            write: message => {
                log.info(message.trim())
            }
        }
    })
);


let staticPath = path.join(__dirname, `../${process.env.PUBLIC_PATH}/`);
app.use("/", express.static(staticPath));

// MARK: - Routes

app.get('/', (req, res) => {
    res.send(`Cactus Crawler API`);
});

app.listen(process.env.PORT, () => {
    log.info(`Listen to port ${process.env.PORT}.`);
    Idrometeo.run();
});

// MARK: - CRON

cron.schedule("* * * * *", async () => {
    console.log("xxx");
    // Idrometeo.run();
});
