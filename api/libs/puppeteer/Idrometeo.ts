import puppeteer from "puppeteer";
import path from "path";
import log from "../winston/logger";
import Crawler, { CrawlerScreenShotOptions } from "./Crawler";

export default class Idrometeo extends Crawler {

    static async run() {

        const headless: boolean = process.env.NODE_ENV === "development" ? false : true;
        
        const browser = await puppeteer.launch({
            headless,
            ignoreHTTPSErrors: true,
        });
        const page = await browser.newPage();
        await page.setViewport({
            width: 1920,
            height: 1080,
            deviceScaleFactor: 1,
        });

        await page.goto('https://www.google.com/', { waitUntil: 'domcontentloaded' });

        let screenshotOptions: CrawlerScreenShotOptions = {
            path: path.join('public', 'element.png'),
            selector: '#hplogo',
            padding: 16,
        };

        try {
            await this.screenshotDOMElement(page, screenshotOptions);
        }
        catch(error) {
            log.error(error.message);
        }

        await browser.close();

    }

}