import puppeteer from "puppeteer";

export interface CrawlerScreenShotOptions {
    selector: string;
    path: string;
    padding: number;
}

export default class Crawler {

    static async screenshotDOMElement(page: any, options: CrawlerScreenShotOptions): Promise<void> {

        let { selector, path, padding } = options;
        
        const rect = await page.evaluate(selector => {
            
            const element = document.querySelector(selector);

            if(!element) return null;

            const { x, y, width, height } = element.getBoundingClientRect();
            return {
                left: x,
                top: y,
                width,
                height,
                id: element.id,
            };

        }, selector);

        if(!rect) throw Error(`Could not find element that matches selector: ${selector}.`);

        await page.screenshot({
            path,
            clip: {
                x: rect.left - padding,
                y: rect.top - padding,
                width: rect.width + padding * 2,
                height: rect.height + padding * 2
            }
        });

    }

}