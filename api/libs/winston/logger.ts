import path from "path";
import fs from "fs";
import winston from "winston";

const FOLDER = `${__dirname}/${path.join("..", "..", "logs")}`;

// Create folder if not exists
if(!fs.existsSync(FOLDER)) fs.mkdirSync(FOLDER);

/*
  https://github.com/winstonjs/winston/blob/master/docs/transports.md
  
  Log level:
  error: 0
  warn: 1 
  info: 2 
  verbose: 3
  debug: 4
  silly: 5 
*/

const logger = winston.createLogger({
    level: 'info', // Log only if [name].level less than or equal to this level
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: `${FOLDER}/error.log`, level: 'error' }),
        new winston.transports.File({ 
            filename: `${FOLDER}/combined.log`,
            maxsize: 5242880, // 5MB
            maxFiles: 10,
        })
    ],
});

if(process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}

export default logger;
